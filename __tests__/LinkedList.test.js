const { list } = require('../LinkedList');

describe('linked list tests', () => {
  const emptyList = list();
  const list1 = emptyList.add('1');
  const list13 = list1.append('3');
  const list123 = list13.addAt('2', 1);
  const list123_2 = list('1', '2', '3');

  it('should be empty', () => {
    expect(emptyList.isEmpty()).toBe(true);
  });

  it('should throw error on head of empty list', () => {
    expect(() => emptyList.head()).toThrowError('Head of empty list');
  });

  it('should throw error on tail of empty list', () => {
    expect(() => emptyList.tail()).toThrowError('Tail of empty list');
  });

  it('should render empty list', () => {
    expect(emptyList.toString()).toBe('[]');
  });

  it('shouldn\'t be empty', () => {
    expect(list1.isEmpty()).toBe(false);
  });

  it('should return size equals 1', () => {
    expect(list1.size()).toBe(1);
  });

  it('should return element on idx 0', () => {
    expect(list1.getItem(0)).toBe('1');
  });

  it('should return size equals 2', () => {
    expect(list13.size()).toBe(2);
  });

  it('should return element at idx 1', () => {
    expect(list13.getItem(1)).toBe('3');
  });

  it('should throw error on get at index >= length', () => {
    expect(() => list13.getItem(2)).toThrowError('Index out of range');
  });

  it('should throw error on get at negative index', () => {
    expect(() => list13.getItem(-1)).toThrowError('Index out of range');
  });

  it('addAt should work at the far end of the list (index === length)', () => {
    expect(list13.size()).toBe(2);
    const result = list13.addAt('10', 2);
    expect(result.size()).toBe(3);
    expect(result.getItem(2)).toBe('10');
    expect(result.toString()).toBe('[1, 3, 10]');
  });

  it('addAt should throw error if index > length', () => {
    expect(() => list13.addAt('1', 3)).toThrowError('Index out of range');
  });

  it('should return size equals 3', () => {
    expect(list123.size()).toBe(3);
  });

  it('should return another element on idx 1', () => {
    expect(list123.getItem(1)).toBe('2');
  });

  it('should return element on idx 2', () => {
    expect(list123.getItem(2)).toBe('3');
  });

  it('should create a list with 1, 2, 3', () => {
    expect(list123_2).toEqual(list123);
  });

  it('should render sitring', () => {
    expect(list123_2.toString()).toBe('[1, 2, 3]');
  });
});