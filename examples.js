import { empty, list } from './LinkedList';

const linkedList = list(1, 2, 3, 4, 5, 6);

console.log(linkedList.toString());

const anotherList = empty().add(3).add(1).addAt(2, 1).append(5).addAt(4, 3);

console.log(anotherList.toString());
